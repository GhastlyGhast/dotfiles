# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

cat ~/.cache/wal/sequences
# Path to your oh-my-zsh installation.
ZSH="/usr/share/oh-my-zsh/"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes

ZSH_THEME="cypher"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
 COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
HIST_STAMPS="dd.mm.yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git archlinux man z web-search dirhistory zsh-autosuggestions zsh-syntax-highlighting)
# common-aliases 
export ZSH_AUTOSUGGEST_STRATEGY='completion'
# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
export EDITOR='emacs'

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

ZSH_CACHE_DIR=$HOME/.cache/oh-my-zsh
if [[ ! -d $ZSH_CACHE_DIR ]]; then
  mkdir $ZSH_CACHE_DIR
fi

source $ZSH/oh-my-zsh.sh

export PATH="$HOME/.local/override:$PATH:$HOME/.local/bin:$HOME/.cabal/bin:$HOME/go/bin"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export MANROFFOPT='-c'
setopt correct
export SPROMPT="Correct $fg[yellow]%R$reset_color to $fg[green]%r$reset_color? [Yes, No, Abort, Edit] "
export WWW_HOME="https://duckduckgo.com"

function mycli()
{
    sudo systemctl start mariadb.service;
    command mycli "$@";
    sudo systemctl stop mariadb.service;
}

function scream()
{
    s=$(for i in {1..$1}; do echo -n "A"; done)
    t=$(for i in {1..$1}; do echo -n "H"; done)
    figlet -t -f slant "$s$t" | lolcat
}

function zzz()
{
    s=$(for i in {1..$1}; do echo -n "Z"; done)
    figlet -t -f slant "$s" | lolcat
}

function spage()
{
    web_search startpage $1 &
}

function emacsfunc()
{
    (emacsclient --frame-parameters="((internal-border-width . 20))" -c $1 && foot) &
    disown
    exit
}


function emacswin()
{
    (emacsclient --frame-parameters="((internal-border-width . 20))" -c $1) &
    disown
    exit
}

alias neofetch='neofetch --disable GPU'
alias please='sudo $(fc -ln -1)'
alias pridetrain='sl | lolcat'
alias welcome='figlet Welcome to ArchLinux && neofetch'
alias psycow='fortune | cowsay'
alias aaahhh='figlet -t -f slant AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHHHHHHHHH | lolcat'
alias pet='clear && ternimal length=100 thickness=2,4,1,0,0 radius=6,11 gradient=0:#22ee88,0.5:#884467,1:#ff004f'
alias vim='nvim'
alias emacsserver='/usr/bin/emacs --daemon'
alias emt='emacsclient -nw'
alias emw='emacsfunc'
alias emacs='emw'
alias em='emacs'
alias sp='spage'
alias blip='img2sixel'

if [[ -n "$DISPLAY" ]]; 
then
    export TERM=xterm-256color
    alias ls='lsd'
fi
function start-conda()
{
    __conda_setup="$('/opt/anaconda/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
    if [ $? -eq 0 ]; then
        eval "$__conda_setup"
    else
        if [ -f "/opt/anaconda/etc/profile.d/conda.sh" ]; then
            . "/opt/anaconda/etc/profile.d/conda.sh"
        else
            export PATH="/opt/anaconda/bin:$PATH"
        fi
    fi
    unset __conda_setup
}

# opam configuration
[[ ! -r /home/virgil/.opam/opam-init/init.zsh ]] || source /home/virgil/.opam/opam-init/init.zsh  > /dev/null 2> /dev/null


export PATH="$HOME/.ghcup/bin:$PATH"

export PATH="$HOME/.elan/bin:$PATH"

zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:warnings' format '%BNo matches'
