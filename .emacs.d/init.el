(setq gc-cons-threshold (* 50 1000 1000))

(require 'package)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)

(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package)
  )
(setq use-package-compute-statistics t)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auth-source-save-behavior nil)
 '(connection-local-criteria-alist
   '(((:application tramp)
      tramp-connection-local-default-system-profile tramp-connection-local-default-shell-profile)
     ((:application eshell)
      eshell-connection-default-profile)))
 '(connection-local-profile-alist
   '((tramp-connection-local-darwin-ps-profile
      (tramp-process-attributes-ps-args "-acxww" "-o" "pid,uid,user,gid,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" "-o" "state=abcde" "-o" "ppid,pgid,sess,tty,tpgid,minflt,majflt,time,pri,nice,vsz,rss,etime,pcpu,pmem,args")
      (tramp-process-attributes-ps-format
       (pid . number)
       (euid . number)
       (user . string)
       (egid . number)
       (comm . 52)
       (state . 5)
       (ppid . number)
       (pgrp . number)
       (sess . number)
       (ttname . string)
       (tpgid . number)
       (minflt . number)
       (majflt . number)
       (time . tramp-ps-time)
       (pri . number)
       (nice . number)
       (vsize . number)
       (rss . number)
       (etime . tramp-ps-time)
       (pcpu . number)
       (pmem . number)
       (args)))
     (tramp-connection-local-busybox-ps-profile
      (tramp-process-attributes-ps-args "-o" "pid,user,group,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" "-o" "stat=abcde" "-o" "ppid,pgid,tty,time,nice,etime,args")
      (tramp-process-attributes-ps-format
       (pid . number)
       (user . string)
       (group . string)
       (comm . 52)
       (state . 5)
       (ppid . number)
       (pgrp . number)
       (ttname . string)
       (time . tramp-ps-time)
       (nice . number)
       (etime . tramp-ps-time)
       (args)))
     (tramp-connection-local-bsd-ps-profile
      (tramp-process-attributes-ps-args "-acxww" "-o" "pid,euid,user,egid,egroup,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" "-o" "state,ppid,pgid,sid,tty,tpgid,minflt,majflt,time,pri,nice,vsz,rss,etimes,pcpu,pmem,args")
      (tramp-process-attributes-ps-format
       (pid . number)
       (euid . number)
       (user . string)
       (egid . number)
       (group . string)
       (comm . 52)
       (state . string)
       (ppid . number)
       (pgrp . number)
       (sess . number)
       (ttname . string)
       (tpgid . number)
       (minflt . number)
       (majflt . number)
       (time . tramp-ps-time)
       (pri . number)
       (nice . number)
       (vsize . number)
       (rss . number)
       (etime . number)
       (pcpu . number)
       (pmem . number)
       (args)))
     (tramp-connection-local-default-shell-profile
      (shell-file-name . "/bin/sh")
      (shell-command-switch . "-c"))
     (tramp-connection-local-default-system-profile
      (path-separator . ":")
      (null-device . "/dev/null"))
     (eshell-connection-default-profile
      (eshell-path-env-list))))
 '(coq-accept-proof-using-suggestion 'never)
 '(custom-safe-themes
   '("046a2b81d13afddae309930ef85d458c4f5d278a69448e5a5261a5c78598e012" "d445c7b530713eac282ecdeea07a8fa59692c83045bf84dd112dd738c7bcad1d" "fee7287586b17efbfda432f05539b58e86e059e78006ce9237b8732fde991b4c" "78e6be576f4a526d212d5f9a8798e5706990216e9be10174e3f3b015b8662e27" default))
 '(haskell-interactive-popup-errors nil)
 '(org-format-latex-options
   '(:foreground default :background default :scale 2.0 :html-foreground "Black" :html-background "Transparent" :html-scale 1.0 :matchers
                 ("begin" "$1" "$" "$$" "\\(" "\\[")))
 '(package-selected-packages
   '(company-lsp dap-java dap-mode lsp-java lsp-ui realgud-jdb vterm all-the-icons eldoc-box nu-mode solarized-theme gruvbox-theme zig-mode obsidian typst-mode exec-path-from-shell esup julia-mode eglot-jl use-package diff-hl magit auctex flycheck company utop dune haskell-mode monokai-theme darcula-theme tuareg treesit-auto package-safe-delete aggressive-indent smartparens paxedit slime))
 '(proof-splash-enable nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :extend nil :stipple nil :background "#161718" :foreground "#F8F8F2" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight regular :height 95 :width normal :foundry "SRC" :family "Hack Nerd Font"))))
 '(eglot-diagnostic-tag-unnecessary-face ((t (:background "color-23"))))
 '(eglot-inlay-hint-face ((t (:inherit shadow :height 1.0))))
 '(flymake-error ((t (:background "color-52"))))
 '(flymake-warning ((t (:background "color-136"))))
 '(markdown-header-face-3 ((t (:inherit markdown-header-face :height 1.02))))
 '(proof-locked-face ((t (:extend t :background "#252525")))))

(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
  backup-by-copying t    ; Don't delink hardlinks
  version-control t      ; Use version numbers on backups
  delete-old-versions t  ; Automatically delete excess backups
  kept-new-versions 20   ; how many of the newest versions to keep
  kept-old-versions 5    ; and how many of the old
  )

(setq auto-save-file-name-transforms
  `((".*" "~/.emacs.d/autosaves/" t)))


(setq load-path (cons "~/.emacs.d/lean4-mode" load-path))
(setq load-path (cons "~/.emacs.d/tamarin" load-path))
(setq load-path (cons "~/.emacs.d/nusmv" load-path))

(setq lean4-mode-required-packages '(dash f flycheck lsp-mode magit-section s))

(let ((need-to-refresh t))
  (dolist (p lean4-mode-required-packages)
    (when (not (package-installed-p p))
      (when need-to-refresh
        (package-refresh-contents)
        (setq need-to-refresh nil))
      (package-install p))))

(require 'lean4-mode)
(require 'spthy-mode)
(require 'nusmv-mode)

(defun prelude-start-slime ()
  (unless (slime-connected-p)
    (progn
      (save-excursion (slime))
      (window-swap-states)
      )
    )
  )

(use-package slime
  :ensure t
  :config
  (setq inferior-lisp-program "sbcl")  
  :hook
  (slime-mode . prelude-start-slime)
  )

(use-package company
  :ensure t
  :config
  (setq company-idle-delay 0.75)
  :hook
  (prog-mode . company-mode)
  )

(use-package eldoc
  :config
  (setq eldoc-idle-delay 0.75)
  )

(use-package eldoc-box
  :ensure t
  :hook
  (eldoc-mode . eldoc-box-hover-at-point-mode) 
  )

(use-package flymake
  :defer t
  :config
  (setq flymake-no-changes-timeout 0.5)
  )

(use-package eglot
  :commands eglot
  :bind
  ("<f2>" . eglot-rename)
  ("<f5>" . xref-find-definitions)
  ("<f6>" . xref-find-definitions-other-window)
  ("<f11>" . flymake-goto-prev-error)
  ("<f12>" . flymake-goto-next-error)
  :config
  (setq eglot-events-buffer-size 0)
  (setq eglot-confirm-server-initiated-edits nil)
  (add-to-list 'eglot-server-programs
                    '(coq-mode . ("coq-lsp")))
   (setq-default eglot-workspace-configuration
                '((haskell
                   (formattingProvider . "ormolu"
                     )))
                )


  )

;; Input of unicode symbols
(use-package math-symbol-lists
  :ensure t
  :hook
  (coq-mode . (lambda () (set-input-method "math")))
  )
                                        ; Automatically use math input method for Coq files
                                        ; Input method for the minibuffer
(defun my-inherit-input-method ()
  "Inherit input method from `minibuffer-selected-window'."
  (let* ((win (minibuffer-selected-window))
         (buf (and win (window-buffer win))))
    (when buf
      (activate-input-method (buffer-local-value 'current-input-method buf)))))
(add-hook 'minibuffer-setup-hook #'my-inherit-input-method)
                                        ; Define the actual input method
(quail-define-package "math" "UTF-8" "Ω" t)
(quail-define-rules ; add whatever extra rules you want to define here...
 ("\\fun"    ?λ)
 ("\\mult"   ?⋅)
 ("\\ent"    ?⊢)
 ("\\valid"  ?✓)
 ("\\diamond" ?◇)
 ("\\box"    ?□)
 ("\\bbox"   ?■)
 ("\\later"  ?▷)
 ("\\pred"   ?φ)
 ("\\and"    ?∧)
 ("\\or"     ?∨)
 ("\\comp"   ?∘)
 ("\\ccomp"  ?◎)
 ("\\all"    ?∀)
 ("\\ex"     ?∃)
 ("\\to"     ?→)
 ("\\sep"    ?∗)
 ("\\lc"     ?⌜)
 ("\\rc"     ?⌝)
 ("\\Lc"     ?⎡)
 ("\\Rc"     ?⎤)
 ("\\lam"    ?λ)
 ("\\empty"  ?∅)
 ("\\Lam"    ?Λ)
 ("\\Sig"    ?Σ)
 ("\\-"      ?∖)
 ("\\aa"     ?●)
 ("\\af"     ?◯)
 ("\\auth"   ?●)
 ("\\frag"   ?◯)
 ("\\iff"    ?↔)
 ("\\gname"  ?γ)
 ("\\incl"   ?≼)
 ("\\latert" ?▶)
 ("\\update" ?⇝)

 ;; accents (for iLöb)
 ("\\\"o" ?ö)

 ;; subscripts and superscripts
 ("^^+" ?⁺) ("__+" ?₊) ("^^-" ?⁻)
 ("__0" ?₀) ("__1" ?₁) ("__2" ?₂) ("__3" ?₃) ("__4" ?₄)
 ("__5" ?₅) ("__6" ?₆) ("__7" ?₇) ("__8" ?₈) ("__9" ?₉)

 ("__a" ?ₐ) ("__e" ?ₑ) ("__h" ?ₕ) ("__i" ?ᵢ) ("__k" ?ₖ)
 ("__l" ?ₗ) ("__m" ?ₘ) ("__n" ?ₙ) ("__o" ?ₒ) ("__p" ?ₚ)
 ("__r" ?ᵣ) ("__s" ?ₛ) ("__t" ?ₜ) ("__u" ?ᵤ) ("__v" ?ᵥ) ("__x" ?ₓ)
)
(mapc (lambda (x)
        (if (cddr x)
            (quail-defrule (cadr x) (car (cddr x)))))
      ; need to reverse since different emacs packages disagree on whether
      ; the first or last entry should take priority...
      ; see <https://mattermost.mpi-sws.org/iris/pl/46onxnb3tb8ndg8b6h1z1f7tny> for discussion
      (reverse (append math-symbol-list-basic math-symbol-list-extended)))

(setq coq-smie-user-tokens
      '(("," . ":=")
	("∗" . "->")
	("-∗" . "->")
	("∗-∗" . "->")
	("==∗" . "->")
	("=∗" . "->") 			;; Hack to match ={E1,E2}=∗
	("|==>" . ":=")
	("⊢" . "->")
	("⊣⊢" . "->")
	("↔" . "->")
	("←" . "<-")
	("→" . "->")
	("=" . "->")
	("==" . "->")
	("/\\" . "->")
	("⋅" . "->")
	(":>" . ":=")
	("by" . "now")
	("forall" . "now")              ;; NB: this breaks current ∀ indentation.
        ))

;java stuff

(use-package scala-mode
  :ensure t
  :interpreter ("scala" . scala-mode))

;; Enable sbt mode for executing sbt commands
(use-package sbt-mode
  :ensure t
  :commands sbt-start sbt-command
  :config
  ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
  ;; allows using SPACE when in the minibuffer
  (substitute-key-definition
   'minibuffer-complete-word
   'self-insert-command
   minibuffer-local-completion-map)
   ;; sbt-supershell kills sbt-mode:  https://github.com/hvesalai/emacs-sbt-mode/issues/152
   (setq sbt:program-options '("-Dsbt.supershell=false")))


(use-package lsp-mode
  :ensure t
  :config
  :bind
  ("<f2>" . lsp-rename)
  ("<f5>" . xref-find-definitions)
  ("<f6>" . xref-find-definitions-other-window)
)

(use-package lsp-ui
  :ensure t
  )

(use-package lsp-java
  :ensure t
  :config
  (add-hook 'java-mode-hook 'lsp)
  (add-hook 'java-mode-hook (lambda () (setq-local indent-tabs-mode nil)))
  (add-hook 'java-mode-hook (lambda () (setq-local c-indentation-style "bsd" c-basic-offset 4)))
  (setq lsp-java-server-install-dir "~/.local/share/jdtls/")
  (setq lsp-java-format-settings-url (lsp--path-to-uri "~/.emacs.d/javaFormatter.xml"))
  )

(use-package lsp-metals
  :ensure t
  :config (add-hook 'scala-mode-hook 'lsp)
  )

(use-package dap-mode
  :ensure t
  :after lsp-mode
  :config (dap-auto-configure-mode)
  )

(use-package dap-java
  :ensure nil
  )

;End of java section

(use-package tex
   :demand t
   :ensure auctex
   :config
   (add-to-list 'TeX-view-program-selection '(output-pdf "Zathura"))
   (setq TeX-PDF-mode t)
   (setq TeX-auto-save t)
   (setq TeX-parse-self t)
   )

(use-package pandoc-mode
  :ensure t
  :hook
  (markdown-mode . pandoc-mode)
)

(use-package typst-mode
  :ensure t
 )

(use-package obsidian
  :ensure t
  :demand t
  :config
  (obsidian-specify-path "~/Documents/Obsidian")
  (global-obsidian-mode nil)
  :custom
  ;; This directory will be used for `obsidian-capture' if set.
  (obsidian-inbox-directory "Zettelkasten")
  :bind (:map obsidian-mode-map
  ;; Replace C-c C-o with Obsidian.el's implementation. It's ok to use another key binding.
  ("C-c C-o" . obsidian-follow-link-at-point)
  ;; Jump to backlinks
  ("C-c C-b" . obsidian-backlink-jump)
  ;; If you prefer you can use `obsidian-insert-link'
  ("C-c C-l" . obsidian-insert-wikilink))
  )

; (use-package typst-mode
;  :ensure t
; )
    
(use-package windmove
  :config
  (windmove-default-keybindings)
  )

(use-package undo-tree
  :ensure t
  :after evil
  :diminish
  :config
  (evil-set-undo-system 'undo-tree)
  (global-undo-tree-mode 1)
  (setq undo-tree-auto-save-history t)
  (setq undo-tree-history-directory-alist '(("." . "~/.emacs.d/undo")))
  :hook
  (coq-mode . (lambda () (undo-tree-mode 1)))
  ) 

(use-package evil
  :ensure t
  :config
  (evil-mode 1)
  )

(use-package neotree
  :ensure t
  :bind ("<f4>" . neotree-toggle)
  )

(use-package diff-hl
  :ensure t
  :hook
  (prog-mode . diff-hl-mode)
  (prog-mode . diff-hl-flydiff-mode)
  (prog-mode . diff-hl-margin-mode)
  )

(setq-default indent-tabs-mode nil)

(use-package tuareg
  :ensure t
  :defer t)

(use-package utop
  :defer t
  :config
  :hook
  (tuareg-mode . utop-minor-mode)
  )

(use-package haskell-mode
  :ensure t
  :hook
  (haskell-mode . (lambda () (set-input-method "Agda")))
  :defer t
)

(use-package idris-mode
  :ensure t
  :config
  :defer t
  :custom
  (idris-interpreter-path "idris2")
)

(use-package proof-general
  :ensure t
  :config
  (setq proof-three-window-mode-policy 'hybrid)
  (global-prettify-symbols-mode 1)
  )


;(global-prettify-symbols-mode 1)

;(setq coq-symbols-list '(lambda ()
;        (mapc (lambda (pair) (push pair prettify-symbols-alist))
;            '(("forall". ?∀)
;              ("exists". ?∃)
;              
;              ("/\\" . ?⋀)
;              ("\\/" . ?⋁)
;              ("=>". ?⇒)
;              ("->". ?→)
;              ("<-" . ?←)
;              ("<->" . ?↔)
;              ("nat" . ?ℕ)
;              ))))

;; Prettify Coq script editor

;(add-hook 'coq-mode-hook coq-symbols-list)

;; Prettify Coq output in proofs
;(add-hook 'coq-goals-mode-hook coq-symbols-list)

;; Prettify Coq output in proofs
;(add-hook 'coq-response-mode-hook coq-symbols-list)


(defun more-symbolz ()
                     (setq-local prettify-symbols-alist
                                 (append prettify-symbols-alist
                                 '(("Alpha" . ?Α) ("Beta" . ?Β) ("Gamma" . ?Γ)
                                   ("Delta" . ?Δ) ("Epsilon" . ?Ε) ("Zeta" . ?Ζ)
                                   ("Eta" . ?Η) ("Theta" . ?Θ) ("Iota" . ?Ι)
                                   ("Kappa" . ?Κ) ("Lambda" . ?Λ) ("Mu" . ?Μ)
                                   ("Nu" . ?Ν) ("Xi" . ?Ξ) ("Omicron" . ?Ο)
                                   ("Pi" . ?Π) ("Rho" . ?Ρ) ("Sigma" . ?Σ)
                                   ("Tau" . ?Τ) ("Upsilon" . ?Υ) ("Phi" . ?Φ)
                                   ("Chi" . ?Χ) ("Psi" . ?Ψ) ("Omega" . ?Ω)
                                   ("alpha" . ?α) ("beta" . ?β) ("gamma" . ?γ)
                                   ("delta" . ?δ) ("epsilon" . ?ε) ("zeta" . ?ζ)
                                   ("eta" . ?η) ("theta" . ?θ) ("iota" . ?ι)
                                   ("kappa" . ?κ) ("lambda" . ?λ) ("mu" . ?μ)
                                   ("nu" . ?ν) ("xi" . ?ξ) ("omicron" . ?ο)
                                   ("pi" . ?π) ("rho" . ?ρ) ("sigma" . ?σ)
                                   ("tau" . ?τ) ("upsilon" . ?υ) ("phi" . ?φ)
                                   ("chi" . ?χ) ("psi" . ?ψ) ("omega" . ?ω)
                                   (":=" . ?≜)
                                   ("Proof." . ?∵)
                                   ("Qed." . ?■)
                                   ("Defined." . ?□)
                                   ("~" . ?¬) ("*" . ?×)
                                   ("~~" . "¬¬") ("~~~" . "¬¬¬")
                                   ("\\in" . ":")
                                   )
                                 )
                                 )
                     )

(use-package company-coq
  :after (proof-general company)
  :ensure t
  :hook
  (coq-mode . company-coq-mode)
  (coq-mode . more-symbolz)
  )

(use-package julia-mode
  :ensure t
  :defer t
  )

(use-package zig-mode
  :ensure t
)

(use-package eglot-jl
  :ensure t
  :hook
  (julia-mode . eglot-jl-init)
  )

(use-package projectile
  :ensure t)

(use-package dashboard
  :ensure t
  :demand t
  :config
  (dashboard-setup-startup-hook)
  (setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))
  (setq dashboard-startup-banner "~/.emacs.d/emacs_logo.txt");;3)
  (setq dashboard-center-content t)
  (setq dashboard-items '((recents  . 5)
                          (bookmarks . 5)
                          (projects . 5))
        )
  (setq dashboard-set-init-info t)
)

(use-package ewal
  :ensure t
  :init (setq ewal-use-built-in-always-p nil
              ewal-use-built-in-on-failure-p t
              ewal-built-in-palette "sexy-material"))

(use-package all-the-icons
  :ensure t
  :if (display-graphic-p)
  :config
  (setq neo-theme 'icons)
  )

(defun wal-update (frame)
  (set-face-background 'default (ewal-load-color 'background) frame)
  (set-face-background 'line-number (ewal-load-color 'background) frame)
  (set-face-background 'line-number-current-line (ewal-load-color 'background) frame)
  (set-face-background 'fringe (ewal-load-color 'background) frame)
  (set-face-background 'internal-border (ewal-load-color 'background) frame)
  (set-face-background 'mode-line (ewal-load-color 'background +1) frame)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)

  )

(defun term-theme (frame)
  (set-face-background 'default "undefined" frame)
  (set-face-background 'line-number "undefined" frame)
  (set-face-background 'line-number-current-line "undefined" frame)
  (set-face-background 'fringe "undefined" frame)
  (set-face-background 'mode-line "undefined" frame)

  )

(defun pick-theme (frame)
  (if (window-system frame)
    (wal-update frame)
    (term-theme frame)
    )
  )

(defun choose-theme ()
  (mapc #'disable-theme custom-enabled-themes)
  (let ((current-theme (f-read-text "~/.config/hypr/current_theme.txt")))
    (cond
     ((string-match-p "day" current-theme) (enable-theme 'gruvbox-dark-hard))
     (t (enable-theme 'monokai))
     )
    )
  )

(defun update-themes ()
  (interactive)
  (choose-theme)
  (dolist (f (frame-list))
    (pick-theme f)
    )
 )

(use-package monokai-theme
  :ensure t
  :config
  (load-theme 'monokai t t)
)

(use-package gruvbox-theme
  :ensure t
  :config
  (load-theme 'gruvbox-dark-hard t t)
  (keymap-set special-event-map "<sigusr1>" 'update-themes)
  (add-hook 'after-make-frame-functions 'pick-theme)
  (update-themes)

)

(use-package solarized-theme
  :ensure t
  :config
)

(use-package exec-path-from-shell
  :ensure t
  :demand t
  :config
  (exec-path-from-shell-initialize)
)
  
(menu-bar-mode -1)

(setq esup-depth 0)

(setq gc-cons-threshold (* 10 1000 1000))

(setq display-line-numbers-type 'relative)
(setq column-number-mode t)
(setq c-default-style "bsd" c-basic-offset 4)
(add-hook 'prog-mode-hook 'display-line-numbers-mode)
(add-hook 'org-mode-hook 'display-line-numbers-mode)

(load-file (let ((coding-system-for-read 'utf-8))
            (shell-command-to-string "agda-mode locate")))

(require 'agda-input)
