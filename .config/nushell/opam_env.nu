let ret = (opam env | split row "\n")

for $x in $ret {
       let cmds = ($x | split row ';')
       let spl = ($cmds.0 | split row '=')
       let var = $spl.0;
       let val = ($spl.1 | split row "'").1 
       load-env {$var : $val}
    }