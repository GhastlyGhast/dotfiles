#!/usr/bin/env bash

while true; do
    RESULT=$(xset -q)
    if  $(echo $RESULT | grep -q "timeout: 0"); then
        echo "%{T1} Off%{T-}"
    else
        echo "%{T1} On%{T-}"
    fi
    sleep 1
done

