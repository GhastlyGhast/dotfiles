#!/bin/sh

STATUS=$(playerctl --player=spotify status 2>/dev/null) 
PAUSED_ICON="  " 
PLAYING_ICON="  "

if [ "$STATUS" = "Playing" ]; then
    echo $PLAYING_ICON
else
    echo $PAUSED_ICON
fi
