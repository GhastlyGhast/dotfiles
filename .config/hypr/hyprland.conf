# See https://wiki.hyprland.org/Configuring/Monitors/
monitor=,preferred,auto,auto


# See https://wiki.hyprland.org/Configuring/Keywords/ for more

# Execute your favorite apps at launch
# exec-once = waybar & hyprpaper & firefox

# Source a file (multi-file configs)
# source = ~/.config/hypr/myColors.conf

# Set programs that you use
$terminal = foot
$fileManager = thunar
$menu = $HOME/.config/hypr/scripts/app_menu.sh
#$menu = wofi --show drun -a --location bottom_left --width 250 --xoffset 10 --height 300 --yoffset -10 -I -M fuzzy
$powermenu = $HOME/.config/hypr/scripts/power_menu.sh

# Some default env vars.
env = XCURSOR_SIZE,24
env = QT_QPA_PLATFORMTHEME,qt5ct # change to qt6ct if you have that

# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {
    kb_layout = fr
    kb_variant =
    kb_model =
    kb_options =
    kb_rules =

    follow_mouse = 2
    float_switch_override_focus = 0

    touchpad {
        natural_scroll = yes
        disable_while_typing = false
    }

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
}

source=~/.config/hypr/theme.conf

general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    gaps_in = 5
    gaps_out = 20
    border_size = 1

    col.inactive_border = rgba(59595955)

    layout = dwindle

    # Please see https://wiki.hyprland.org/Configuring/Tearing/ before you turn this on
    allow_tearing = false
    no_focus_fallback = true
}

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    rounding = 10
    
    blur {
        enabled = true
        size = 3
        passes = 1
    }

    drop_shadow = yes
    shadow_range = 4
    shadow_render_power = 3
    col.shadow = rgba(1a1a1aee)
}


animations {
    enabled = yes

    # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

    bezier = myBezier, 0.05, 0.9, 0.1, 1.05

    animation = windows, 1, 3, myBezier
    animation = windowsOut, 1, 3, default, popin 80%
    animation = border, 1, 10, default
    animation = borderangle, 1, 8, default
    animation = fade, 1, 3, default
    animation = workspaces, 1, 3, default
}

dwindle {
    # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
    pseudotile = yes # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
    preserve_split = yes # you probably want this
}

master {
    # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
    new_is_master = true
}

gestures {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    workspace_swipe = off
}

misc {
    # enable_swallow = true
    # swallow_regex = ^(foot)$
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    force_default_wallpaper = 0 # Set to 0 or 1 to disable the anime mascot wallpapers
    mouse_move_focuses_monitor = false
    disable_hyprland_logo = true
    disable_splash_rendering = true
}

binds {
    workspace_back_and_forth = true
}

#xwayland {
#    force_zero_scaling = true
#}

# Example per-device config
# See https://wiki.hyprland.org/Configuring/Keywords/#executing for more

# Example windowrule v1
# windowrule = float, ^(kitty)$
# Example windowrule v2
windowrulev2 = float,class:^(Rofi)$
# See https://wiki.hyprland.org/Configuring/Window-Rules/ for more

windowrulev2 = suppressevent maximize, class:.* # You'll probably like this.

windowrulev2 = workspace 2 silent,class:^(LibreWolf)$ 
windowrulev2 = workspace 3 silent,class:^(discord)$ 
windowrulev2 = opacity 0.8, focus:0, class:^foot$
windowrulev2 = opacity 0.8, focus:0, class:^emacs$
# See https://wiki.hyprland.org/Configuring/Keywords/ for more
$mainMod = SUPER

# Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
bind = $mainMod, RETURN, exec, $terminal
bind = $mainMod, X, killactive, 
bind = $mainMod, D, exec, $menu
bind = $mainMod SHIFT, E, exec, $powermenu
bind = $mainMod, SPACE, togglefloating, 
bind = $mainMod, F, fullscreen, 
bind = $mainMod, P, pseudo, # dwindle
bind = $mainMod, J, togglesplit, # dwindle

# Move focus with mainMod + arrow keys
bind = $mainMod, left, movefocus, l
bind = $mainMod, right, movefocus, r
bind = $mainMod, up, movefocus, u
bind = $mainMod, down, movefocus, d

bind = $mainMod SHIFT, left, movewindow, l
bind = $mainMod SHIFT, right, movewindow, r
bind = $mainMod SHIFT, up, movewindow, u
bind = $mainMod SHIFT, down, movewindow, d

# will switch to a submap called resize
bind=$mainMod,R,submap,resize

# will start a submap called "resize"
submap=resize

# sets repeatable binds for resizing the active window
binde=,right,resizeactive,10 0
binde=,left,resizeactive,-10 0
binde=,up,resizeactive,0 -10
binde=,down,resizeactive,0 10

# use reset to go back to the global submap
bind=,escape,submap,reset 
bind=$mainMod,R,submap,reset 

# will reset the submap, meaning end the current one and return to the global one
submap=reset


# Switch workspaces with mainMod + [0-9]
bind = $mainMod, ampersand, workspace, 1
bind = $mainMod, eacute, workspace, 2
bind = $mainMod, quotedbl, workspace, 3
bind = $mainMod, apostrophe, workspace, 4
bind = $mainMod, parenleft, workspace, 5
bind = $mainMod, minus, workspace, 6
bind = $mainMod, egrave, workspace, 7
bind = $mainMod, underscore, workspace, 8
bind = $mainMod, ccedilla, workspace, 9
bind = $mainMod, agrave, workspace, 10

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, ampersand, movetoworkspacesilent, 1
bind = $mainMod SHIFT, eacute, movetoworkspacesilent, 2
bind = $mainMod SHIFT, quotedbl, movetoworkspacesilent, 3
bind = $mainMod SHIFT, apostrophe, movetoworkspacesilent, 4
bind = $mainMod SHIFT, parenleft, movetoworkspacesilent, 5
bind = $mainMod SHIFT, minus, movetoworkspacesilent, 6
bind = $mainMod SHIFT, egrave, movetoworkspacesilent, 7
bind = $mainMod SHIFT, underscore, movetoworkspacesilent, 8
bind = $mainMod SHIFT, ccedilla, movetoworkspacesilent, 9
bind = $mainMod SHIFT, agrave, movetoworkspacesilent, 10

# Example special workspace (scratchpad)
bind = $mainMod, S, togglespecialworkspace, magic
bind = $mainMod SHIFT, S, movetoworkspace, special:magic

bind = $mainMod SHIFT, D, exec, ~/.config/hypr/scripts/set-theme.sh day 
bind = $mainMod SHIFT, N, exec, ~/.config/hypr/scripts/set-theme.sh night
bind = $mainMod SHIFT, O, exec, ~/.config/hypr/scripts/set-theme.sh neon 

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

bind = CONTROL Mod1, SPACE, exec, dunstctl close

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow

bindel=, XF86AudioRaiseVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+
bindel=, XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-
bindl=, XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
bindl=, XF86AudioMicMute, exec, wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle
bindl=, XF86MonBrightnessUp, exec, brightnessctl -q s 5%+
bindl=, XF86MonBrightnessDown, exec, brightnessctl -q s 5%-

#Hack for forcing correct order of exec
exec-once = xrdb ~/.Xresources; ~/.config/hypr/scripts/init-theme.sh; /bin/emacs --daemon; waybar
exec-once = hyprpaper
exec-once = dunst
exec-once = /usr/lib/polkit-kde-authentication-agent-1

xwayland {
  force_zero_scaling = true
}

# toolkit-specific scale
#env = GDK_SCALE,2
#env = XCURSOR_SIZE,32
