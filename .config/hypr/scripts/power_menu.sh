#!/bin/bash
COMMAND="$(rofi -sep "|" -dmenu -i -p 'System' -location 5 -xoffset -20 -yoffset -20  -hide-scrollbar -line-padding 4 -padding 20 -theme-str 'window {width: 250; lines: 1; height: 270;}' -show-icons -icon-theme "Papirus" -font "Fantasque Mono 14" -normal-window <<< "  Lock|  Logout| Suspend|⏾  Hibernate|  Reboot|  Shutdown")"
case "$COMMAND" in
    *Lock) swaylock ;;
    *Logout) $HOME/.config/hypr/scripts/force-logout.sh;; #loginctl kill-session self;;
    *Suspend) systemctl suspend;;
    *Hibernate) systemctl hibernate;;
    *Reboot) systemctl reboot ;;
    *Shutdown) systemctl poweroff
esac
