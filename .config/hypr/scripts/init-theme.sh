#!/bin/sh

theme=$(cat ~/.config/hypr/current_theme.txt)
/bin/sh ~/.config/hypr/scripts/set-theme.sh $theme
