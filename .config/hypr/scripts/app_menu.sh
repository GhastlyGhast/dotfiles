#!/bin/bash

## Created By Aditya Shakya

rofi -sort -modi run,drun -show drun -location 7 -xoffset 20 -yoffset -20 -line-padding 4 -columns 1 -theme-str 'window {width: 500; lines: 10;}' -padding 25 -hide-scrollbar -show-icons -drun-icon-theme -font "Fantasque Sans Mono 14" -normal-window
