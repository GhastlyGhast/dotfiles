#!/bin/sh

killall -q picom

while pgrep -u $UID -x picom >/dev/null; do sleep 0.1; done

if [ $# -ne 0 ]
then
    option=$1
else
    option="round"
fi

if [ "$option" = "round" ]
then
    picom &
else
    picom --corner-radius 0.0 &
fi
