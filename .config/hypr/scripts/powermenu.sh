#!/bin/bash

PROMPT="  Lock\n  Logout\n Suspend\n⏾  Hibernate\n  Reboot\n  Shutdown"

COMMAND=$(echo -en "$PROMPT" | wofi -c .config/wofi/powermenu -s .config/wofi/powermenu.css --show dmenu --location bottom_right --yoffset -10 --height 180 --width 100 --xoffset -100)

case "$COMMAND" in
    *Lock) swaylock ;;
    *Logout) $HOME/.config/hypr/scripts/force-logout.sh;; #loginctl kill-session self;;
    *Suspend) systemctl suspend;;
    *Hibernate) systemctl hibernate;;
    *Reboot) systemctl reboot ;;
    *Shutdown) systemctl poweroff
esac
