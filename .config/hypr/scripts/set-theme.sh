#!/bin/sh

theme=$1
bg=""
border=""


if [ "$theme" = "day" ]
then
    bg="~/Pictures/Wallpapers/canyon.jpg"
    border="0xffc6530d"
elif [ "$theme" = "night" ]
then
    bg="~/Pictures/Wallpapers/astronaut.png"
    border="0xff387459"
elif [ "$theme" = "neon" ]
then
    bg="~/Pictures/Wallpapers/neon-lines.jpg"
    border="0xff33219a"
else
    exit 1
fi

/usr/bin/wal -t -f "/home/virgil/.config/wal/$theme.json"
/bin/cp "$HOME/.local/share/nwg-look/$theme""_gsettings" "$HOME/.local/share/nwg-look/gsettings"
/bin/cp "$HOME/.config/waybar/$theme""_colors.css" "$HOME/.config/waybar/colors.css"
if pgrep -x waybar >/dev/null; then
    killall -SIGUSR2 waybar
fi
/bin/nwg-look -a
/bin/hyprctl hyprpaper wallpaper eDP-1,"$bg"
/bin/cp "$HOME/.config/hypr/$theme""_theme.conf" "$HOME/.config/hypr/theme.conf"
/bin/hyprctl reload
/bin/cp "$HOME/.config/wofi/$theme""_colors" "$HOME/.config/wofi/colors"
#keyword general:col.active_border "$border"
/home/virgil/.local/bin/pywalfox update
/usr/bin/pywal-discord
echo $theme > /home/virgil/.config/hypr/current_theme.txt
killall -s SIGUSR1 emacs
/bin/spicetify restore backup apply
