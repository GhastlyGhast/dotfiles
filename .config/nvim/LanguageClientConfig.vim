let g:deoplete#enable_at_startup = 1
set completeopt-=preview
" autozimu/LanguageClient-neovim config
set hidden

let g:LanguageClient_useVirtualText = "No"

let g:LanguageClient_serverCommands = {
    \'rust': ['rls'],
    \'python': ['pylsp'],
    \'c': ['clangd', '--fallback-style=Microsoft'],
    \'cpp': ['clangd', '--fallback-style=Microsoft'],
    \'haskell': ['haskell-language-server-wrapper', '--lsp'],
    \'java': ['jdtls', '-data ./'],
    \'ocaml' : ['ocamllsp'],
    \'lua' : ['lua-language-server'],
    \'julia': ['julia', '--startup-file=no', '--history-file=no', '-e', '    # Load LanguageServer.jl: attempt to load from ~/.julia/environments/nvim-lspconfig\n    # with the regular load path as a fallback\n    ls_install_path = joinpath(\n        get(DEPOT_PATH, 1, joinpath(homedir(), ".julia")),\n        "environments", "nvim-lspconfig"\n    )\n    pushfirst!(LOAD_PATH, ls_install_path)\n    using LanguageServer\n    popfirst!(LOAD_PATH)\n    depot_path = get(ENV, "JULIA_DEPOT_PATH", "")\n    project_path = let\n        dirname(something(\n            ## 1. Finds an explicitly set project (JULIA_PROJECT)\n            Base.load_path_expand((\n                p = get(ENV, "JULIA_PROJECT", nothing);\n                p === nothing ? nothing : isempty(p) ? nothing : p\n            )),\n            ## 2. Look for a Project.toml file in the current working directory,\n            ##    or parent directories, with $HOME as an upper boundary\n            Base.current_project(),\n            ## 3. First entry in the load path\n            get(Base.load_path(), 1, nothing),\n            ## 4. Fallback to default global environment,\n            ##    this is more or less unreachable\n            Base.load_path_expand("@v#.#"),\n        ))\n    end\n    @info "Running language server" VERSION pwd() project_path depot_path\n    server = LanguageServer.LanguageServerInstance(stdin, stdout, project_path, depot_path)\n    server.runlinter = true\n    run(server)\n  '] 
    \ }
" \'java' : ['java-language-server'],

let g:LanguageClient_diagnosticsDisplay = {
        \1: {
        \   "name": "Error",
        \   "texthl": "LanguageClientError",
        \    "signText": "",
        \    "signTexthl": "LanguageClientErrorSign",
        \    "virtualTexthl": "Error",
        \},
        \2: {
        \    "name": "Warning",
        \    "texthl": "LanguageClientWarning",
        \    "signText": "",
        \    "signTexthl": "LanguageClientWarningSign",
        \    "virtualTexthl": "Todo",
        \ },
        \3: {
        \    "name": "Information",
        \    "texthl": "LanguageClientInfo",
        \    "signText": "ℹ",
        \    "signTexthl": "LanguageClientInfoSign",
        \    "virtualTexthl": "Todo",
        \},
        \4: {
        \    "name": "Hint",
        \    "texthl": "LanguageClientInfo",
        \    "signText": "➤",
        \    "signTexthl": "LanguageClientInfoSign",
        \    "virtualTexthl": "Todo",
        \},
\}

nmap <silent> <F5> <Plug>(lcn-menu)
nmap <silent> <F3> <Plug>(lcn-hover)
nmap <silent> gd <Plug>(lcn-definition)
nmap <silent> <F2> <Plug>(lcn-rename)


inoremap <silent><expr> <TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

"OCaml
set rtp^="/home/virgil/.opam/default/share/ocp-indent/vim"

autocmd BufWritePost *.java LanguageClientStart
