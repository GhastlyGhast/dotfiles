call plug#begin()
Plug 'itchyny/lightline.vim'
Plug 'lervag/vimtex'

Plug 'autozimu/LanguageClient-neovim', {
	    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }

Plug 'junegunn/fzf'
Plug 'airblade/vim-gitgutter'
Plug 'preservim/nerdtree'
Plug 'justinmk/vim-syntax-extra'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'vim-scripts/taglist.vim'
Plug 'lukas-reineke/indent-blankline.nvim'
Plug 'sophacles/vim-processing'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'mrjones2014/nvim-ts-rainbow'
Plug 'RRethy/nvim-base16'
Plug 'vlime/vlime'

" Plug 'frazrepo/vim-rainbow'
" Plug 'mxw/vim-prolog'
" Plug 'luochen1990/rainbow'
call plug#end()

source ~/.config/nvim/LanguageClientConfig.vim
source ~/.config/nvim/theme.vim


map <silent> <F4> :NERDTreeToggle<CR>
nmap <silent> <F6> :TlistToggle<CR>
nmap <silent> <ESC> :let @/ = ""<CR>
nmap <silent> <F7> :set rnu!<CR>
nmap <silent> <F8> :TSBufToggle rainbow<CR>
nmap <silent> <F9> :below split<bar>resize 10<bar>term<CR>
tnoremap <Esc> <C-\><C-n>
" So processes are killed when terminal buffer is closed
set nohidden

filetype plugin indent on
syntax enable
set tabstop=4
set shiftwidth=4
set softtabstop=4
set number
set relativenumber
set expandtab
set smarttab
set autoindent
set mouse=a
let g:c_syntax_for_h = 1

let mapleader="²"
" let g:rainbow_ctermfgs = ['yellow', 'red', 'blue', 'lightblue', 'lightgreen', 'green']

" au FileType ocaml,haskell call rainbow#load()

lua <<EOF
require("nvim-treesitter.configs").setup {
    highlight = {
        enable = true,
        additional_vim_regex_highlighting = false,
    },
    -- ...
    rainbow = {
        enable = true,
        disable = { "ocaml", "haskell" }, -- list of languages you want to disable the plugin for
        extended_mode = true, -- Also highlight non-bracket delimiters like html tags, boolean or table: lang -> boolean
        max_file_lines = nil, -- Do not enable for files with more than n lines, int
        -- colors = {}, -- table of hex strings
        -- termcolors = {} -- table of colour name strings
        }
  }
EOF
let g:vimtex_view_method = 'zathura'

"indent_blankline configuration
let g:indent_blankline_show_trailing_blankline_indent = v:false
let g:indent_blankline_use_treesitter = v:true
let g:indent_blankline_filetype_exclude = [ 
                                            \"lspinfo",
                                            \"packer",
                                            \"checkhealth",
                                            \"help",
                                            \"vim",
                                            \"haskell",
                                            \"prolog",
                                            \"ocaml",
                                            \"",
                                         \]
"lua<< EOF
"require('base16-colorscheme').setup({
"        base00 = '#16161D', base01 = '#2c313c', base02 = '#3e4451', base03 = '#6c7891',
"        base04 = '#565c64', base05 = '#abb2bf', base06 = '#9a9bb3', base07 = '#c5c8e6',
"        base08 = '#e06c75', base09 = '#d19a66', base0A = '#e5c07b', base0B = '#98c379',
"        base0C = '#56b6c2', base0D = '#0184bc', base0E = '#c678dd', base0F = '#a06949',
"})
"EOF


call FitSchemeToTerm()
