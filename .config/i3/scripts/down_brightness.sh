#!/bin/sh

FILE_B="/sys/class/backlight/intel_backlight/brightness"
FILE_MB="/sys/class/backlight/intel_backlight/max_brightness"

BR=$(< $FILE_B)
MBR=$(< $FILE_MB)
NBR=$(($BR - ($MBR / 10)))

echo $NBR > $FILE_B
